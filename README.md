# Bin generator

[![PyPI version](https://img.shields.io/pypi/v/bin-generator)](https://pypi.org/project/bin-generator/)
![License](https://img.shields.io/pypi/l/bin-generator)
![Python versions](https://img.shields.io/pypi/pyversions/bin-generator)

## Installation

This tool can now be installed with pip:
```bash
pip install bin-generator[full]
```

## Example usage

You can see some example data in the `example` folder of the source code repository.
To try them out, use the following command:

```bash
src/bin-generator example/bins.yaml --data example/data.yaml
```

## Known issues

- Interrupting the script will delete the remaining files
